package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
/*
 * This is an helper class of the Car project API called CarList.
 * 
 * Essentially has two methods: getInstance which basically it creates a carList instancing a new CircularSortedDoublyLinkedList of type Car with a comparator on it
 * for the sorting to return and resetCars which that returns the same list but cleared.
 */

public class CarList {
	private static CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return carList;
	}
	public static void resetCars() {
		carList.clear();
	}
}
