package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;
/*
 * This is the comparator of this Car project.
 * 
 * Its purpose is really simple, is to keep all the list involved in this project sorted out alphabetically in order
 * by parsing all the main parts of a node of a Car and using the String compareTo.
 */

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {
		String param1 = o1.getCarBrand() + o1.getCarModel() + o1.getCarModelOption();
		String param2 = o2.getCarBrand() + o2.getCarModel() + o2.getCarModelOption();
		return param1.compareTo(param2);
	}
	

}
