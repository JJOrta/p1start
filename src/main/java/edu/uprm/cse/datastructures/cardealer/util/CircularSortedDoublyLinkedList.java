package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
/*
 * This is the Circular Sorted Doubly Linked List of this Car project.
 * 
 * It starts with an Iterator implementation, this tool is important since it allow us to look through
 * CircularSortedDoublyLinkedList list without changing it, great for the contains method, which looks for
 * a generic element in the list, in these case cars and the methods firstIndex and lastIndex which as the name
 * gives out, searches the first node and the last node respectively of a CircularSortedDoublyLinkedList list.
 * 
 * For the rest of the parts its just standard implementation of the CircularSortedDoublyLinkedList class, with
 * a very unique add that tackles various situations: the first one is when the CircularSortedDoublyLinkedListIterator
 * is empty,then when the add can occur between the last element of the CSDLL and the header, followed by the case
 * between the header and the next of it and lastly in the while, between the next node of the header and the previous node
 * of it.
 */
public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> curr;
		
		public CircularSortedDoublyLinkedListIterator() {
			this.curr = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return this.curr.getElement() != null;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E itr = this.curr.getElement();
				this.curr = this.curr.getNext();
				return itr;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	private static class Node<E>{
		private E element;
		private Node <E> next;
		private Node <E> prev;
		public Node() {
			this.next = null;
			this.prev = null;
			this.element = null;
		}
		public Node(E d, Node<E> n, Node<E> p) {
			this.element = d;
			this.next = n;
			this.prev = p;
		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node prev) {
			this.prev = prev;
		}
	}
		private Node<E> header;
		private int currentSize;
		private Comparator<E> comparator;
		public CircularSortedDoublyLinkedList(Comparator<E> comparator){
			this.header = new Node<>();
			this.currentSize = 0;
			this.comparator = comparator;
			
			this.header.setNext(this.header.getNext());
		}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

		@Override
		public boolean add(E e) {	
			if(this.isEmpty()) {
				Node<E> newNode = new Node<E>(e, this.header, this.header);
				this.header.setNext(newNode);
				this.header.setPrev(newNode);
				newNode.setPrev(this.header);
				newNode.setNext(this.header);
				this.currentSize++;
				return true;
			}else{
				Node<E> newNode2 = new Node<E>(e, null, null);
				
				Node<E> headerPrevNode = this.header.getPrev();
				
				Node<E> currentNode = this.header.getNext();
				
				if(comparator.compare(e, currentNode.getElement()) <= 0) {
					this.header.setNext(newNode2);
					currentNode.setPrev(newNode2);
					newNode2.setNext(currentNode);
					newNode2.setPrev(this.header);
					this.currentSize++;
					return true;
				}
				
				if(comparator.compare(e, headerPrevNode.getElement()) >= 0) {
					newNode2.setNext(this.header);
					newNode2.setPrev(headerPrevNode);
					headerPrevNode.setNext(newNode2);
					this.header.setPrev(newNode2);
					this.currentSize++;
					return true;
				}
				
				
				while(currentNode.getElement() != null) {
					
					currentNode = currentNode.getNext();
					Node<E> prevNode = currentNode.getPrev();
					
					if(comparator.compare(e, currentNode.getElement()) <= 0) {
						prevNode.setNext(newNode2);
						currentNode.setPrev(newNode2);
						newNode2.setNext(currentNode);
						newNode2.setPrev(prevNode);
						this.currentSize++;
						return true;
					}
				}
			}
			return false;	
		}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if(i < 0) {
			return false;
		}
		else {
			this.remove(i);
			return true;
		}
	}

	@Override
	public boolean remove(int index) {
		
		if((index < 0 )|| (index >= this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}
		else {
			Node<E> temp = this.header;
			int currentIndex = 0;
			Node<E> target = null;
			while(currentIndex != index) {
				temp = temp.getNext();
				currentIndex++;
			}

			target = temp.getNext();
			temp.setNext(target.getNext());
			target.getNext().setPrev(temp);
			target.setElement(null);
			target.setNext(null);
			target.setPrev(null);
			this.currentSize--;
			return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.remove(obj)) {
			count++;
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E first() {
		// TODO Auto-generated method stub
		return header.getNext().getElement();
	}

	@SuppressWarnings("unchecked")
	@Override
	public E last() {
		// TODO Auto-generated method stub
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		
		Node<E> temp = this.getIndex(index);
		return temp.getElement();
	}

	private Node<E> getIndex(int index) {
		int currentIndex = 0;
		Node<E> temp = this.header.getNext();
		while(currentIndex != index) {
			temp = temp.getNext();
			currentIndex++;
		}
		return temp;
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
		
	}

	@Override
	public boolean contains(E e) {
		Iterator <E> iter = new CircularSortedDoublyLinkedListIterator<E>();
		E iterElement;
		while(iter.hasNext()) {
			iterElement = iter.next();
			if(iterElement.equals(e)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) {
		int count = 0;
		Iterator <E> iter = new CircularSortedDoublyLinkedListIterator<E>();
		E iterElement;
		while(iter.hasNext()) {
			iterElement = iter.next();
			if(iterElement.equals(e)) {
				return count;
			}
			count++;
		}
		return -1;
		
	}

	@Override
	public int lastIndex(E e) {
		int count = 0;
		int result = 0;
		Iterator <E> iter = new CircularSortedDoublyLinkedListIterator<E>();
		E iterElement;
		while(iter.hasNext()) {
			iterElement = iter.next();
			if(iterElement.equals(e)) {
				result = count;
			}
			count++;
		}
		if(result == 0) {
			return -1;
		}
		else {
			return result;
		}
	}

	@Override
	public E[] toArray() {
		E[] result = (E[]) new Object[this.size()];
		int i = 0;
		Node<E> temp = this.header.getNext();
		while(temp != this.header) {
			result[i] = (E) temp.getElement();
			i++;
			temp = temp.getNext();
		}
		return result;
	}

}
