package edu.uprm.cse.datastructures.cardealer;

import java.util.ArrayList;

import java.util.Comparator;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

/*
 * the API of the Car project called CarManager. It uses the CSDLL tools and some maven, JSON, grizzly tools.
 * 
 * One of its methods its getAllCars: which gets the all the cars of a CarList, then getCar which looks for a single car
 * given an id and throws an exception if its not found. After those we find addCar, which adds a car to a CSDLL list and returns
 * an OK response, then we have updateCar which deletes the old information and puts a new one and deleteCar which
 * deletes the car. Both last two methods return an OK status if the function is performed and a NOT_FOUND if the function is not performed.
 */
@Path("/cars")
public class CarManager {
	private final SortedList<Car> cList = CarList.getInstance();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars(){
		if(cList.size() == 0) {
			return new Car[1];
		}
		else {
			Car[] result = new Car[cList.size()];
			for(int i = 0; i < cList.size(); ++i) {
				result[i] = cList.get(i);
			}
			return result;
		}
	}
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for(Car allCars : cList) {
			if(allCars.getCarId() == id) {
				return allCars;
			}
		}
		throw new NotFoundException(new JsonError("Error", "Customer" + id + "not found"));
	}
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		cList.add(car);
		return Response.status(201).build();
	}
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		int count = 0;
		for(Car allCars : cList) {
			if(allCars.getCarId() == car.getCarId()) {
				cList.remove(count);
				cList.add(car);
				
				return Response.status(Response.Status.OK).build();
			}
			count++;
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		int count = 0;
		
		for(Car allCars: cList) {
			if(allCars.getCarId() == id) {;
				cList.remove(count);
				return Response.status(Response.Status.OK).build();
			}
			count++;
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
